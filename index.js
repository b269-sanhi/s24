//  Get Cube
const num = 8 ** 3;
console.log(`The cube of 8 is ${num}`);

// Address
let address = [258 , 'Washington ave NW', 'California', 90011 ]
let  [streetNumber, streetName, stateName, postalCode] = address;
console.log('I live at ' + ' ' + streetNumber + ' ' + streetName + ', ' + stateName + ' ' + postalCode);

// Animal
let animal = [1075 , 20, 3 ]
let  [weight, height, heightIn] = animal;
console.log('Lolong was a salt water crocodile. ' + ' ' + 'He weighed at' + ' ' + weight + ' ' + 'kgs' + ' ' + 'with a measurement of' + ' ' + height + ' ' + 'ft' + ' ' + heightIn + ' ' + 'in'); 

// Array of number

let digits = [1,2,3,4,5,15]
digits.forEach((digit) => {
	console.log(`${digit}`)
	});

// Reduce number
let numbers = [1, 2, 3, 4, 5];

let sum = numbers.reduce(
  (x, y) => x + y
);

console.log(sum);

// Dog
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};
const myDog = new Dog('Cali', 2, 'Teacup');
console.log(myDog);
